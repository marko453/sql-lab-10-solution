# Lab 10 - Stress Testing

Student name: <b>Marko Pezer</b><br>
Student email: <b>m.pezer@innopolis.university</b><br>
Student group: <b>BS18-SE-01</b>

## Screenshots

![Report](/screenshots/report.JPG)
<br><br>
![Terminal](/screenshots/terminal.JPG)